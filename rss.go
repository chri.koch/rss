//representation of a rss feed
package rss

import (
	"encoding/xml"
)

//here starts the original rss feed
type Rss struct {
	XMLName xml.Name `xml:"rss"`
	Version string   `xml:"version,attr"`
	Channel RssChannel
}

type RssChannel struct {
	XMLName     xml.Name  `xml:"channel"`
	Title       string    `xml:"title"`
	Link        string    `xml:"link"`
	Description string    `xml:"description"`
	Language    string    `xml:"language"`
	Copyright   string    `xml:"copyright"`
	PubDate     string    `xml:"pubDate"`
	Image       RssImage  `xml:"image"`
	Items       []RssItem `xml:"item"`
}

type RssImage struct {
	XMLName xml.Name `xml:"image"`
	Url     string   `xml:"url"`
	Title   string   `xml:"title"`
	Link    string   `xml:"link"`
}

type RssItem struct {
	XMLName   xml.Name `xml:"item"`
	Title     string   `xml:"title"`
	Desc      string   `xml:"description"`
	Link      string   `xml:"link"`
	Author    string   `xml:"author"`
	Guid      RssItemGuid
	PubDate   string `xml:"pubDate"`
	Enclosure RssItemEnclosure
}

type RssItemGuid struct {
	XMLName     xml.Name `xml:"guid"`
	IsPermaLink string   `xml:"isPermaLink,attr"`
	Guid        string   `xml:",chardata"`
}

type RssItemEnclosure struct {
	XMLName xml.Name `xml:"enclosure"`
	Url     string   `xml:"url,attr"`
}
